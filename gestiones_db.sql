-- Crear la base de datos "gestiones_db"
CREATE DATABASE gestiones_db;

-- Configurar codificación de caracteres a UTF-8
ALTER DATABASE gestiones_db
  SET client_encoding = 'UTF8';

-- Configurar collation y tipo de orden (opcional)
ALTER DATABASE gestiones_db
  SET default_text_search_config = 'pg_catalog.spanish';

-- Crear el usuario "db_user" con la contraseña "abc123"
CREATE USER db_user WITH PASSWORD 'abc123';

-- Opcional: Configurar otras propiedades de la base de datos si es necesario

-- Crear la tabla "usuario_financiero"
CREATE TABLE usuario_financiero (
    id SERIAL PRIMARY KEY,
    fecha_creacion TIMESTAMP,
    usuario_creacion VARCHAR(100) NOT NULL,
    fecha_modificacion TIMESTAMP,
    usuario_modificacion VARCHAR(100) NOT NULL
);

-- Crear la tabla "persona_juridica"
CREATE TABLE persona_juridica (
    id SERIAL PRIMARY KEY,
    id_usuario_financiero INT,
    nombres VARCHAR(100) NOT NULL,
    razon_social VARCHAR(100) NOT NULL,
    nit VARCHAR(20) NOT NULL,
    direccion VARCHAR(60) NOT NULL,
    telefono VARCHAR(20) NOT NULL
);

-- Crear la tabla "persona_natural"
CREATE TABLE persona_natural (
    id SERIAL PRIMARY KEY,
    id_usuario_financiero INT,
    nombres VARCHAR(100) NOT NULL,
    primer_apellido VARCHAR(60) NOT NULL,
    segundo_apellido VARCHAR(60) NOT NULL,
    nro_documento_identidad VARCHAR(20) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    direccion_domicilio VARCHAR(40) NOT NULL
);

-- Insertar datos en la tabla "usuario_financiero"
INSERT INTO usuario_financiero (fecha_creacion, usuario_creacion, fecha_modificacion, usuario_modificacion)
VALUES ('2023-09-27 10:00:00', 'usuario_creador', '2023-09-27 10:30:00', 'usuario_modificador');

-- Insertar datos en la tabla "persona_juridica"
INSERT INTO persona_juridica (id_usuario_financiero, nombres, razon_social, nit, direccion, telefono)
VALUES (1, 'christian', 'Razón Social XYZ', '1234567890', 'Cochabamba', '123-456-7890');

-- Otorgar permisos al usuario "db_user"
GRANT SELECT, INSERT, UPDATE, DELETE ON persona_juridica TO db_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON persona_natural TO db_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON usuario_financiero TO db_user;

-- Conceder permisos adicionales
ALTER USER db_user CREATEDB;
ALTER USER db_user WITH SUPERUSER;
