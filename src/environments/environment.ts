export const environment = {
  app_dev: 'Christian Pañuni',
  app_name: 'Gestiones',
  app_version: '0.1-DEMO',
  apiURL_BASE: 'http://localhost:9090/',
  production: false,
  name: 'development',
};
