import { NzIconService } from 'ng-zorro-antd/icon';
import { CustomIconSources } from './custom-icon-source';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CustomIconService {
  constructor(private iconService: NzIconService) {}
  registerIcons() {
    CustomIconSources.forEach((iconSource) => {
      this.iconService.addIcon(iconSource);
    });
  }
}
