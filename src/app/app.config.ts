import * as AllIcons from '@ant-design/icons-angular/icons';
import localeEn from '@angular/common/locales/en';
import localeEs from '@angular/common/locales/es';
import {
  APP_INITIALIZER,
  ApplicationConfig,
  importProvidersFrom,
  LOCALE_ID,
} from '@angular/core';
import { CustomIconService } from 'src/assets/custom-icon.service';
import { en_US, es_ES, NZ_I18N } from 'ng-zorro-antd/i18n';

import {
  HttpClient,
  HttpClientModule,
  provideHttpClient,
} from '@angular/common/http';
import { IconDefinition } from '@ant-design/icons-angular';
import { NZ_CONFIG, NzConfig } from 'ng-zorro-antd/core/config';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import { routes } from './app.routes';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { RxStompService } from './config/rx-stomp.service';

registerLocaleData(localeEs, 'es');
registerLocaleData(localeEn, 'en');

const antDesignIcons = AllIcons as { [key: string]: IconDefinition };
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(
  (key) => antDesignIcons[key]
);

function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

const ngZorroConfig: NzConfig = {
  message: { nzTop: 0, nzDuration: 3000 },
  notification: { nzTop: 240 },
};

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    importProvidersFrom(HttpClientModule),
    importProvidersFrom(NzNotificationModule, NzMessageModule, NzModalModule),
    importProvidersFrom(
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient],
        },
        defaultLanguage: 'es',
      })
    ),
    provideHttpClient(),

    provideAnimations(),
    {
      provide: NZ_I18N,
      useFactory: (localId: string) => {
        switch (localId) {
          case 'en':
            return en_US;
          case 'es':
            return es_ES;
          default:
            return es_ES;
        }
      },
    },
    { provide: NZ_ICONS, useValue: icons },
    { provide: LOCALE_ID, useValue: 'es-BO' },
    { provide: NZ_CONFIG, useValue: ngZorroConfig },
    {
      provide: RxStompService,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (cis: CustomIconService) => () => cis.registerIcons(),
      deps: [CustomIconService],
      multi: true,
    },
  ],
};
