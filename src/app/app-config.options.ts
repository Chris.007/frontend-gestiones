import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
export class ConfigOptions {
  public static apiURL_BASE: string = environment.apiURL_BASE;
  public static siteTitle: string = 'Gestiones';
  public static httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
}
