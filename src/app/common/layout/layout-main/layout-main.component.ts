import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

//NG-ZORRO
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzModalModule } from 'ng-zorro-antd/modal';

import { AppFooterComponent } from '../common/app-footer/app-footer.component';
import { AppSidebarComponent } from '../common/app-sidebar/app-sidebar.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

@Component({
  standalone: true,
  selector: 'ak-layout-main',
  templateUrl: './layout-main.component.html',
  styleUrls: ['./layout-main.component.scss'],
  imports: [
    CommonModule,
    RouterModule,
    NzLayoutModule,
    NzDrawerModule,
    NzButtonModule,
    NzMenuModule,
    NzIconModule,
    NzAffixModule,
    AppSidebarComponent,
    AppFooterComponent,
    NzModalModule,
    HttpClientModule,
  ],
})
export class LayoutMainComponent {
  public isCollapsed = false;

  toggleCollapse(collapsed: boolean): void {
    this.isCollapsed = collapsed;
  }
}
