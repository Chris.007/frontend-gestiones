import { Routes } from '@angular/router';

export const layoutMainRoutes: Routes = [
  { path: '', redirectTo: 'legalPerson', pathMatch: 'full' },
  {
    path: 'legalPerson',
    loadComponent: () =>
      import(
        '../../../modules/userManagement/legalPerson/components/legalPerson-list/legalPerson-list.component'
      ).then((c) => c.LegalPersonListComponent),
  },
];
