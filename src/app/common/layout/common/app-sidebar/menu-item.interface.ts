export interface MenuItem {
  level?: number;
  title?: string;
  icon?: string;
  open?: boolean;
  selected?: boolean;
  disabled?: boolean;
  route?: string;
  roles?: string[]; //importante
  divider?: boolean;
  children?: MenuItem[];
}
