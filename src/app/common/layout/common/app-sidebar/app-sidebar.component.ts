import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MENU_ITEMS } from './menu-items.object';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { RouterModule } from '@angular/router';
import { MenuItem } from './menu-item.interface';

const MENU_ACTIVO = 'MENU_ACTIVO';

@Component({
  selector: 'ak-sidebar',
  standalone: true,
  templateUrl: './app-sidebar.component.html',
  styleUrls: ['./app-sidebar.component.scss'],
  providers: [],
  imports: [CommonModule, RouterModule, NzMenuModule, NzIconModule],
})
export class AppSidebarComponent {
  @Input() akCollapsed = false;
  public menus = MENU_ITEMS;
  private selected = false;
  private opened = false;

  constructor() {
    this.reset(this.menus);
  }

  public reset(menus: MenuItem[]) {
    menus.forEach((m) => {
      if (m.children != undefined) {
        m.open = false;
        m.selected = false;
        this.reset(m.children);
      } else {
        m.selected = false;
      }
    });
  }

  public openMenu(open: boolean) {
    this.opened = open;
    console.log('Nodo abierto: ' + open);
  }

  public selectedMenu(menu: MenuItem) {
    this.search(this.menus, menu);
    this.selected = !this.selected;
    menu.selected = this.selected;
    console.log('Item selecionado: ' + this.selected);
  }

  private search(menus: MenuItem[], menu: MenuItem) {
    if (menu)
      menus.forEach((m) => {
        if (m.children != undefined) {
          this.search(m.children, menu);
        } else {
          if (m.title === menu.title) {
            sessionStorage.removeItem(MENU_ACTIVO);
            sessionStorage.setItem(MENU_ACTIVO, JSON.stringify(m));
            m.selected = true;
          } else {
            m.selected = false;
          }
        }
      });
  }
}

function findParents(
  menus: MenuItem[],
  title: string,
  padres: MenuItem[] = []
): MenuItem[] | null {
  for (const menu of menus) {
    if (menu.title === title) {
      // Hemos encontrado el elemento objetivo, devolvemos la lista de padres.
      return [...padres, menu];
    }

    if (menu.children) {
      const result = findParents(menu.children, title, [...padres, menu]);
      if (result) {
        // Hemos encontrado el elemento objetivo en un subárbol, devolvemos la lista de padres.
        return result;
      }
    }
  }
  // Si no se encuentra el elemento objetivo en este subárbol, devolvemos null.
  return null;
}
