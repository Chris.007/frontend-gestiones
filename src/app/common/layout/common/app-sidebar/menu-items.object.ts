import { MenuItem } from './menu-item.interface';

export const MENU_ITEMS: MenuItem[] = [
  {
    level: 1,
    title: 'Gestion de usuarios',
    icon: 'user',
    open: false,
    selected: false,
    disabled: false,
    route: '',
    roles: [],
    divider: false,
    children: [
      {
        level: 2,
        title: 'Persona Natural',
        icon: 'safety',
        open: false,
        selected: false,
        disabled: false,
        roles: ['ADMIN'],
        route: '/naturalPerson',
      },
      {
        level: 2,
        title: 'Persona Juridica',
        icon: 'ng-zorro:person_apron',
        open: false,
        selected: false,
        disabled: false,
        roles: ['ADMIN'],
        route: '/legalPerson',
      },
    ],
  },
];
