export interface ResponseDTO {
  code: number;
  message: string;
  dto: any;
}
