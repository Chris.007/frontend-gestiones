export class LegalPersonTableDTO {
  constructor(
    public id: number,
    public usuarioFinanciero: any,
    public nombres: string,
    public razon_social: string,
    public nit: string,
    public direccion: string,
    public telefono: string
  ) {
    this.id = id;
    this.usuarioFinanciero = usuarioFinanciero;
    this.nombres = nombres;
    this.razon_social = razon_social;
    this.nit = nit;
    this.direccion = direccion;
    this.telefono = telefono;
  }
}

export class LegalPersonDTO {
  constructor(
    public usuarioFinanciero: any,
    public nombres: string,
    public razon_social: string,
    public nit: string,
    public direccion: string,
    public telefono: string
  ) {
    this.usuarioFinanciero = usuarioFinanciero;
    this.nombres = nombres;
    this.razon_social = razon_social;
    this.nit = nit;
    this.direccion = direccion;
    this.telefono = telefono;
  }
}
