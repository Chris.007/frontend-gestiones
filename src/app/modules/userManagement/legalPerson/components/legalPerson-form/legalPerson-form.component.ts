import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
//NZ ZORRO
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTagModule } from 'ng-zorro-antd/tag';
import {
  NzModalComponent,
  NzModalModule,
  NzModalRef,
} from 'ng-zorro-antd/modal';
import { LegalPersonDTO } from '../../../common/users-models.dto';
import { APIUserService } from '../../common/api-legalPerson.service';
@Component({
  standalone: true,
  selector: 'app-legalPerson-form',
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzSpinModule,
    NzFormModule,
    NzInputModule,
    FormsModule,
    NzDatePickerModule,
    NzSpaceModule,
    NzIconModule,
    NzSpinModule,
    NzGridModule,
    NzButtonModule,
    NzTagModule,
    NzModalModule,
  ],
  templateUrl: './legalPerson-form.component.html',
  styleUrls: ['./legalPerson-form.component.scss'],
})
export class LegalPersonFormComponent implements OnInit {
  public modalRef = inject(NzModalRef);
  public apiService = inject(APIUserService);
  public formGroup: FormGroup = new FormGroup({
    nombres: new FormControl(null),
    razon_social: new FormControl(null),
    nit: new FormControl(null),
    direccion: new FormControl(null),
    telefono: new FormControl(null),
  });
  private legalPersonDTO: LegalPersonDTO = new LegalPersonDTO(
    '',
    '',
    '',
    '',
    '',
    ''
  );
  constructor() {}

  ngOnInit() {}

  public save(): void {
    this.legalPersonDTO = this.formGroup.getRawValue();
    this.legalPersonDTO.usuarioFinanciero = null;
    this.apiService.create(this.legalPersonDTO).subscribe({
      next: (response) => this.modalRef.destroy(response),
      error: (error) => console.log(error),
      complete: () => console.log('Created copleted!'),
    });
  }

  public close(): void {
    this.modalRef.destroy();
  }
}
