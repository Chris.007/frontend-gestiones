import { Component, inject, OnInit } from '@angular/core';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
//NZ-ZORRO
import { NzModalModule, NzModalService } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { LegalPersonFormComponent } from '../legalPerson-form/legalPerson-form.component';
import { ResponseDTO } from 'src/app/common/dtos/common-interface.dto';
import { APIUserService } from '../../common/api-legalPerson.service';
@Component({
  standalone: true,
  selector: 'app-legalPerson-list',
  imports: [
    CommonModule,
    NzTableModule,
    NzDividerModule,
    NzPaginationModule,
    NzPageHeaderModule,
    NzSpaceModule,
    NzButtonModule,
    NzDropDownModule,
    NzIconModule,
    NzToolTipModule,
    NzTagModule,
    HttpClientModule,
    NzModalModule,
  ],
  templateUrl: './legalPerson-list.component.html',
  styleUrls: ['./legalPerson-list.component.scss'],
})
export class LegalPersonListComponent implements OnInit {
  private modalService = inject(NzModalService);
  private apiService = inject(APIUserService);
  public loading = false;
  public dataSource: any[] = [];
  constructor() {}

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this.loading = true;
    this.apiService.getAll().subscribe((data) => {
      this.dataSource = data;
      this.loading = false;
    });
  }

  public new(): void {
    this.modalService.create({
      nzTitle: 'Nueva Persona Juridica',
      nzCentered: true,
      nzContent: LegalPersonFormComponent,
    });
  }

  public delete(data_id: number): void {
    this.modalService.confirm({
      nzTitle: 'Eliminar',
      nzCentered: true,
      nzContent: 'Estas seguro de eliminar?',
      nzOnOk: () => {
        this.apiService.delete(data_id).subscribe({});
      },
    });
  }

  public view(data: number): void {
    this.modalService.create({
      nzTitle: 'Informacio Persona Juridica',
      nzCentered: true,
      nzContent: 'Estamos trabajando en ello..',
    });
  }
  public edit(data: any): void {}
}
