import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
// LOCAL
import { ConfigOptions } from 'src/app/app-config.options';
import {
  LegalPersonDTO,
  LegalPersonTableDTO,
} from '../../common/users-models.dto';
import { ResponseDTO } from 'src/app/common/dtos/common-interface.dto';

@Injectable({
  providedIn: 'root',
})
export class APIUserService {
  private http: HttpClient = inject(HttpClient);
  private url: string = ConfigOptions.apiURL_BASE + 'persona_juridica';
  private httpOptions = ConfigOptions.httpOptions;

  public getAll() {
    return this.http.get<any[]>(
      'http://localhost:9090/persona_juridica/',
      this.httpOptions
    );
  }
  public create(persona_juridica: LegalPersonDTO) {
    return this.http.post<any[]>(
      'http://localhost:9090/persona_juridica/',
      persona_juridica,
      this.httpOptions
    );
  }

  public delete(id: number) {
    return this.http.delete<any[]>(
      `http://localhost:9090/persona_juridica/${id}`,
      //this.url+'/delete/?ids='+ids,
      this.httpOptions
    );
  }

  public update(dto: LegalPersonDTO) {
    return this.http.put<ResponseDTO>(
      `${this.url}/update/`,
      //veriicar
      this.httpOptions
    );
  }
}
