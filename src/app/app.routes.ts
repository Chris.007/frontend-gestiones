import { Routes } from '@angular/router';
import { LayoutMainComponent } from './common/layout/layout-main/layout-main.component';

export const routes: Routes = [
  {
    path: '',
    component: LayoutMainComponent,
    loadChildren: () =>
      import('./common/layout/layout-main/layout-main.routes').then(
        (c) => c.layoutMainRoutes
      ),
  },
];
